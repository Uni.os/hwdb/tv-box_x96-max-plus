---
- test: Armbian_20.09_Arm-64_buster_current_5.8.5.img
  - type: server
  - works: yes
  - post: https://forum.armbian.com/topic/12988-armbian-for-amlogic-s905x3/page/12/?tab=comments#comment-108285

- image: Armbian_20.09_Arm-64_buster_current_5.8.5.img.xz
  - build: September 4
  - stor:
    - sha: 0ec66ef73f43d5fcfbfa2efe127dd440ed6cad84426a51ed30a5c8701997df02
    - origin: https://forum.armbian.com/topic/12162-single-armbian-image-for-rk-aml-aw-aarch64-armv8/
    - path: RK_AML_AW/20.09/20200904
    - ark: Uni.os/Armbian

- config:
  - dtb:
  - u-boot:
