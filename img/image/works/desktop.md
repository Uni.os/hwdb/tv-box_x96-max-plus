---
- test: Armbian_20.09_Arm-64_bullseye_current_5.8.5_desktop.img.xz
  - type: desktop
  - works: yes
  - post: https://forum.armbian.com/topic/12988-armbian-for-amlogic-s905x3/page/12/?tab=comments#comment-108285

- image: Armbian_20.09_Arm-64_bullseye_current_5.8.5_desktop.img.xz
  - build: September 4
  - stor:
    - sha: f84995867be0601a84fe45095b21c58f0420a232f243e5793e88a21d14122c42
    - origin: https://forum.armbian.com/topic/12162-single-armbian-image-for-rk-aml-aw-aarch64-armv8/
    - path: RK_AML_AW/20.09/20200904
    - ark: Uni.os/Armbian

- config:
  - dtb:
  - u-boot:
