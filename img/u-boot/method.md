---
- test: Toothpick & USB 3
  - works: Yes!
  - boot: Toothpick press button in audio port!
  - usb: USB 3.0
    - usb.location: nearest to back
  - comment: Hold until after logo disappears. Screen goes Black for 20+ seconds, before kernel appears! Slower USB drives may take longer.


- test: App Update & USB 3
  - works: no
  - comment: Does not seem to work! with Armbian 20.09 images. Keep trying.
  - boot: Update from App menu, in Android
  - usb: USB3.0
    - usb.location: nearest to back
