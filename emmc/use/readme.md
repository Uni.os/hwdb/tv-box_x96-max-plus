eMMC can be erased Completely! and used in CoreElec /Uni.os.
Make sure I only erase 'mmcblk0'; I have not tried recovery yet!
It will require toothpick for all reboots.

works: yes
command: `mkfs.ext4 -L stor /dev/mmcblk0`
