Sometimes, when this TV Box is Bricked CoreELEC can still Boot, Since it doesn't require u-boot! It's simpler, just a kernel.img & dtb.img. Recovery can then be done with dd & ddbr.

- name: Recovery with CoreELEC
  works: yes
  image: CoreELEC-Amlogic-ng.arm-9.2.4.2-Generic.img.gz
    - dtb: sm1_s905x3_4g_1gbit.dtb
  media: USB
  method: Toothpick
  usb: USB 3.0
  usb.location: closest to back.
